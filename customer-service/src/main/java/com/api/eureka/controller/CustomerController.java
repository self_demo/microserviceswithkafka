package com.api.eureka.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.eureka.records.CustomerRequest;
import com.api.eureka.records.CustomerResponse;
import com.api.eureka.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@PostMapping
	private ResponseEntity<String> createCustomer(@RequestBody @Valid CustomerRequest request) {

		return ResponseEntity.ok(customerService.createCustomer(request));

	}

	@PutMapping
	private ResponseEntity<Void> updateCustomer(@RequestBody @Valid CustomerRequest request) {
		customerService.updateCustomer(request);
		return ResponseEntity.accepted().build();
	}

	@GetMapping
	private ResponseEntity<List<CustomerResponse>> findAllCustomer() {
		return ResponseEntity.ok(customerService.findAllCustomers());
	}

	@GetMapping("/exists/{customer-id}")
	private ResponseEntity<Boolean> existsById(@PathVariable("customer-id") String customerId) {
		return ResponseEntity.ok(customerService.existsById(customerId));
	}

	@GetMapping("/{customer-id}")
	private ResponseEntity<CustomerResponse> customerById(@PathVariable("customer-id") String customerId) {
		return ResponseEntity.ok(customerService.customerById(customerId));
	}

	@DeleteMapping("/{customer-id}")
	private ResponseEntity<Void> deleteCustomerById(
			@PathVariable("customer-id") String customerId) {
		customerService.deleteCustomerById(customerId);
		return ResponseEntity.accepted().build();
	}

}
