package com.api.eureka.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data

public class CustomerNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String msg;

	public String getMsg() {
		return msg;
	}

	public CustomerNotFoundException(String msg) {
		super();
		this.msg = msg;
	}

}
