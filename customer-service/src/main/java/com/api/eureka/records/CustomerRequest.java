package com.api.eureka.records;

import com.api.eureka.document.Address;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;

public record CustomerRequest(String id,

		@NotNull(message = "Customer FirstName Is Required")
		String firstName,

		@NotNull(message = "Customer LastName Is Required")
		String lastName,

		@NotNull(message = "Customer Email Is Required")
		@Email(message = "Customer Email Is Not Valid")
		String email,

		Address address

) {

}
