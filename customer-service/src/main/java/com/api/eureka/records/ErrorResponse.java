package com.api.eureka.records;

import java.util.Map;

public record ErrorResponse(

		Map<String, String> errors

) {

}
