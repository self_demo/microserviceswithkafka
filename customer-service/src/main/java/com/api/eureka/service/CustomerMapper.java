package com.api.eureka.service;

import org.springframework.stereotype.Service;

import com.api.eureka.document.Customer;
import com.api.eureka.records.CustomerRequest;
import com.api.eureka.records.CustomerResponse;

import jakarta.validation.Valid;

@Service
public class CustomerMapper {

	public Customer toCustomer(CustomerRequest request) {
		if (request == null) {
			return null;
		}

		Customer customer = new Customer();
		customer.setId(request.id());
		customer.setFirstName(request.firstName());
		customer.setLastName(request.lastName());
		customer.setEmail(request.email());
		customer.setAddress(request.address());
		return customer;
	}

	public CustomerResponse fromCustomer(Customer customer1) {
		return new CustomerResponse(customer1.getId(), customer1.getFirstName(), customer1.getLastName(),customer1.getEmail(),
				customer1.getAddress());
	}

}
