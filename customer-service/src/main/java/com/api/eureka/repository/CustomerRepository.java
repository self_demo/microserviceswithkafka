package com.api.eureka.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.api.eureka.document.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

}
