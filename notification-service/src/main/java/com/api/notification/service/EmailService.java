package com.api.notification.service;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import com.api.notification.model.EmailTemplates;
import com.api.notification.model.Product;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;

@Service
@RequiredArgsConstructor
@Log4j
public class EmailService {

	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Async
	public void sendPaymentSuccessEmail(String destinationEmail, String customerName, BigDecimal amount,
			String orderReference) throws MessagingException {

		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage,
				MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
		messageHelper.setFrom("nicolas17mail@gmail.com");

		final String templateName = EmailTemplates.PAYMENT_CONFIRMATION.getTemplate();

		Map<String, Object> variables = new HashMap<>();
		variables.put("customerName", customerName);
		variables.put("amount", amount);
		variables.put("orderReference", orderReference);

		Context context = new Context();
		context.setVariables(variables);
		messageHelper.setSubject(EmailTemplates.PAYMENT_CONFIRMATION.getSubject());

		try {
			String htmlTemplate = templateEngine.process(templateName, context);
			messageHelper.setText(htmlTemplate, true);

			messageHelper.setTo(destinationEmail);
			mailSender.send(mimeMessage);
			logger.info(String.format("INFO - Email successfully sent to %s with template %s ", destinationEmail,
					templateName));
		} catch (MessagingException e) {
			logger.warn("WARNING - Cannot send Email to {} ", destinationEmail);
		}

	}

	@Async
	public void sendOrderConfirmationEmail(String destinationEmail, String customerName, BigDecimal amount,
			String orderReference, List<Product> products) throws MessagingException {

		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage,
				MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
		messageHelper.setFrom("nicolas17mail@gmail.com");

		final String templateName = EmailTemplates.ORDER_CONFIRMATION.getTemplate();

		Map<String, Object> variables = new HashMap<>();
		variables.put("customerName", customerName);
		variables.put("totalAmount", amount);
		variables.put("orderReference", orderReference);
		variables.put("products", products);

		Context context = new Context();
		context.setVariables(variables);
		messageHelper.setSubject(EmailTemplates.ORDER_CONFIRMATION.getSubject());

		try {
			String htmlTemplate = templateEngine.process(templateName, context);
			messageHelper.setText(htmlTemplate, true);

			messageHelper.setTo(destinationEmail);
			mailSender.send(mimeMessage);
			logger.info(String.format("INFO - Email successfully sent to %s with template %s ", destinationEmail,
					templateName));
		} catch (MessagingException e) {
			logger.warn("WARNING - Cannot send Email to {} ", destinationEmail);
		}

	}

}
