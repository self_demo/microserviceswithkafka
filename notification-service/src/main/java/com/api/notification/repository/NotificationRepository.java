package com.api.notification.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.api.notification.entity.Notification;

@Repository
public interface NotificationRepository extends MongoRepository<Notification, String> {

}
