package com.api.notification.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.api.notification.kafka.OrderConfirmation;
import com.api.notification.kafka.PaymentConfirmation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Document
public class Notification {

	@Id
	private String id;
	private NotificationType type;
	private LocalDateTime notificationDate;
	private OrderConfirmation orderConfirmation;
	private PaymentConfirmation paymentConfirmation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public LocalDateTime getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(LocalDateTime notificationDate) {
		this.notificationDate = notificationDate;
	}

	public OrderConfirmation getOrderConfirmation() {
		return orderConfirmation;
	}

	public void setOrderConfirmation(OrderConfirmation orderConfirmation) {
		this.orderConfirmation = orderConfirmation;
	}

	public PaymentConfirmation getPaymentConfirmation() {
		return paymentConfirmation;
	}

	public void setPaymentConfirmation(PaymentConfirmation paymentConfirmation) {
		this.paymentConfirmation = paymentConfirmation;
	}

}