package com.api.notification.kafka;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.api.notification.entity.Notification;
import com.api.notification.entity.NotificationType;
import com.api.notification.repository.NotificationRepository;
import com.api.notification.service.EmailService;

import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationConsumer {

	private static final Logger logger = LoggerFactory.getLogger(NotificationConsumer.class);

	@Autowired
	private NotificationRepository notificationRepository;
	@Autowired
	private EmailService emailService;

	@KafkaListener(topics = "payment-topic")
	public void consumePaymentSuccessNotifications(PaymentConfirmation paymentConfirmation) throws MessagingException {
		logger.info(String.format("Consuming the message from payment-topic Topic:: %s", paymentConfirmation));

		Notification notification = new Notification();
		notification.setType(NotificationType.PAYMENT_CONFIRMATION);
		notification.setNotificationDate(LocalDateTime.now());
		notification.setPaymentConfirmation(paymentConfirmation);

		notificationRepository.save(notification);
		var customerName = paymentConfirmation.customerFirstname() + " " + paymentConfirmation.customerLastname();
		System.err.println(customerName+" "+ "payment-topic");
		emailService.sendPaymentSuccessEmail(paymentConfirmation.customerEmail(), customerName,
				paymentConfirmation.amount(), paymentConfirmation.orderReference());

	}

	@KafkaListener(topics = "order-topic")
	public void consumeOrderConfirmationNotifications(OrderConfirmation orderConfirmation) throws MessagingException {
		logger.info(String.format("Consuming the message from order-topic Topic:: %s", orderConfirmation));

		Notification notification = new Notification();
		notification.setType(NotificationType.PAYMENT_CONFIRMATION);
		notification.setNotificationDate(LocalDateTime.now());
		notification.setOrderConfirmation(orderConfirmation);

		notificationRepository.save(notification);
		var customerName = orderConfirmation.customer().firstname() + " " + orderConfirmation.customer().lastname();
		System.err.println(customerName+" "+ "order-topic");
		emailService.sendOrderConfirmationEmail(orderConfirmation.customer().email(), customerName,
				orderConfirmation.totalAmount(), orderConfirmation.orderReference(), orderConfirmation.products());

	}
}
