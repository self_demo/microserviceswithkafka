package com.api.notification.kafka;

import java.math.BigDecimal;

import com.api.notification.entity.PaymentMethod;

public record PaymentConfirmation(String orderReference, BigDecimal amount, PaymentMethod paymentMethod,
		String customerFirstname, String customerLastname, String customerEmail) {

}
