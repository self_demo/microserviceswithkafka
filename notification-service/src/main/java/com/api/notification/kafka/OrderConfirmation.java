package com.api.notification.kafka;

import java.math.BigDecimal;
import java.util.List;

import com.api.notification.entity.PaymentMethod;
import com.api.notification.model.Customer;
import com.api.notification.model.Product;

public record OrderConfirmation(String orderReference, BigDecimal totalAmount, PaymentMethod paymentMethod,
		Customer customer, List<Product> products) {

}
