package com.api.order.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.order.exceptions.BusinessException;
import com.api.order.feignClient.CustomerClient;
import com.api.order.kafka.OrderProducer;
import com.api.order.model.OrderConfirmation;
import com.api.order.model.OrderLineRequest;
import com.api.order.model.OrderRequest;
import com.api.order.model.PurchaseRequest;
import com.api.order.repositories.OrderRepository;
import com.api.order.resttemplate.ProductClient;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	@Autowired
	private OrderRepository repository;
	@Autowired
	private OrderMapper mapper;
	@Autowired
	private CustomerClient customerClient;
//	@Autowired
//	private PaymentClient paymentClient;
	@Autowired
	private ProductClient productClient;
	@Autowired
	private OrderLineService orderLineService;
	@Autowired
	private OrderProducer orderProducer;

	@Transactional
	public Integer createOrder(OrderRequest request) {
		var customer = this.customerClient.findCustomerById(request.customerId()).orElseThrow(
				() -> new BusinessException("Cannot create order:: No customer exists with the provided ID"));

		//below is in resttemplate
		var purchasedProducts = productClient.purchaseProducts(request.products());

		var order = this.repository.save(mapper.toOrder(request));

		for (PurchaseRequest purchaseRequest : request.products()) {
			orderLineService.saveOrderLine(
					new OrderLineRequest(null, order.getId(), purchaseRequest.productId(), purchaseRequest.quantity()));
		}
//		var paymentRequest = new PaymentRequest(request.amount(), request.paymentMethod(), order.getId(),
//				order.getReference(), customer);
//		paymentClient.requestOrderPayment(paymentRequest);
//
		orderProducer.sendOrderConfirmation(new OrderConfirmation(request.reference(), request.amount(),
				request.paymentMethod(), customer, purchasedProducts));

		return order.getId();
	}

//	public List<OrderResponse> findAllOrders() {
//		return this.repository.findAll().stream().map(this.mapper::fromOrder).collect(Collectors.toList());
//	}
//
//	public OrderResponse findById(Integer id) {
//		return this.repository.findById(id).map(this.mapper::fromOrder).orElseThrow(
//				() -> new EntityNotFoundException(String.format("No order found with the provided ID: %d", id)));
//	}
}