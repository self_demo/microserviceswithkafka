package com.api.order.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.api.order.entities.Order;
import com.api.order.model.OrderConfirmation;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderProducer {

	private static final Logger logger = LoggerFactory.getLogger(OrderProducer.class);

	@Autowired
	private KafkaTemplate<String, OrderConfirmation> kafkaTemplate;

	public void sendOrderConfirmation(OrderConfirmation orderConfirmation) {
		logger.info("Sending order confirmation");
		Message<OrderConfirmation> message = MessageBuilder.withPayload(orderConfirmation)
				.setHeader(KafkaHeaders.TOPIC, "order-topic").build();

		kafkaTemplate.send(message);
	}
}
