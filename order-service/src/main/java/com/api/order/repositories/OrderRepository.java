package com.api.order.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.order.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

}
