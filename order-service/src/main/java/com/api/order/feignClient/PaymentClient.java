package com.api.order.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.api.order.model.PaymentRequest;

@FeignClient(name = "payment-url", url = "${application.config.payment-url}")
public interface PaymentClient {

	 @PostMapping
	  Integer requestOrderPayment(@RequestBody PaymentRequest request);
}
