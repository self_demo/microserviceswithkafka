package com.api.order.feignClient;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import com.api.order.model.CustomerResponse;

@FeignClient(name = "customer-service", url = "${application.config.customer-url}")
public interface CustomerClient {

	@GetMapping("/{customer-id}")
	Optional<CustomerResponse> findCustomerById(@PathVariable("customer-id") String customerId);

}
