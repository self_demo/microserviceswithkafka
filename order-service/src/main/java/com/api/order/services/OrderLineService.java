package com.api.order.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.order.model.OrderLineRequest;
import com.api.order.model.OrderLineResponse;
import com.api.order.repositories.OrderLineRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderLineService {

	@Autowired
	private OrderLineRepository repository;
	@Autowired
	private OrderLineMapper mapper;

	public Integer saveOrderLine(OrderLineRequest orderLineRequest) {

		var order = mapper.toOrderLine(orderLineRequest);
		return repository.save(order).getId();
	}

	  public List<OrderLineResponse> findAllByOrderId(Integer orderId) {
	        return repository.findAllByOrderId(orderId)
	                .stream()
	                .map(mapper::toOrderLineResponse)
	                .collect(Collectors.toList());
	    }

}
