package com.api.order.services;

import org.springframework.stereotype.Service;

import com.api.order.entities.Order;
import com.api.order.entities.OrderLine;
import com.api.order.model.OrderLineRequest;
import com.api.order.model.OrderLineResponse;

@Service
public class OrderLineMapper {

	public OrderLine toOrderLine(OrderLineRequest orderLineRequest) {
		OrderLine orderLine = new OrderLine();
		orderLine.setId(orderLineRequest.orderId());
		orderLine.setProductId(orderLineRequest.productId());
		Order order = new Order();
		order.setId(orderLineRequest.orderId());
		orderLine.setOrder(order);
		orderLine.setQuantity(orderLineRequest.quantity());
		return orderLine;
	}
	
	   public OrderLineResponse toOrderLineResponse(OrderLine orderLine) {
	        return new OrderLineResponse(
	                orderLine.getId(),
	                orderLine.getQuantity()
	        );
	    }

}
