package com.api.order.model;

import java.math.BigDecimal;

import com.api.order.entities.PaymentMethod;


public record PaymentRequest(BigDecimal amount, PaymentMethod paymentMethod, Integer orderId,
		String orderReference, CustomerResponse customer) {

}
