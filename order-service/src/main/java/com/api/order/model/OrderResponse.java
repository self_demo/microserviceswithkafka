package com.api.order.model;

import java.math.BigDecimal;

import com.api.order.entities.PaymentMethod;

public record OrderResponse(Integer id, String reference, BigDecimal amount, PaymentMethod paymentMethod,
		String customerId) {

}
