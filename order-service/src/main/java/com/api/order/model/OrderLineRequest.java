package com.api.order.model;

public record OrderLineRequest(Integer id, Integer orderId, Integer productId, double quantity) {

}
