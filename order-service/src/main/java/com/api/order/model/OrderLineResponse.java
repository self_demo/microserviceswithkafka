package com.api.order.model;

public record OrderLineResponse(Integer id, double quantity) {

}
