package com.api.order.model;

import java.math.BigDecimal;
import java.util.List;

import com.api.order.entities.PaymentMethod;

public record OrderConfirmation(
		
		  String orderReference,
	        BigDecimal totalAmount,
	        PaymentMethod paymentMethod,
	        CustomerResponse customer,
	        List<PurchaseResponse> products) {

}
