package com.api.order.entities;

public enum PaymentMethod {
	PAYPAL, CREDIT_CARD, VISA, MASTER_CARD, BITCOIN
}
