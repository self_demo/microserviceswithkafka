package com.api.payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.payment.controller.record.PaymentRequest;
import com.api.payment.controller.service.PaymentService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/payments")
@RequiredArgsConstructor
public class PaymentController {

	@Autowired
	private PaymentService service;

	@PostMapping
	public ResponseEntity<Integer> createPayment(@RequestBody @Valid PaymentRequest request) {
		return ResponseEntity.ok(this.service.createPayment(request));
	}
}