package com.api.payment.controller.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.payment.entitiy.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

}
