package com.api.payment.controller.record;

import java.math.BigDecimal;

import com.api.payment.entitiy.PaymentMethod;

public record PaymentNotificationRequest(String orderReference, BigDecimal amount, PaymentMethod paymentMethod,
		String customerFirstname, String customerLastname, String customerEmail) {

}
