package com.api.payment.controller.record;

import java.math.BigDecimal;

import com.api.payment.entitiy.PaymentMethod;

public record PaymentRequest(Integer id, BigDecimal amount, PaymentMethod paymentMethod, Integer orderId,
		String orderReference, Customer customer) {

}
