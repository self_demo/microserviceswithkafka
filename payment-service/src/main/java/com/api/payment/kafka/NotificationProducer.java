package com.api.payment.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.api.payment.controller.record.PaymentNotificationRequest;

import static org.springframework.kafka.support.KafkaHeaders.TOPIC;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationProducer {

	private static final Logger logger = LoggerFactory.getLogger(NotificationProducer.class);

	@Autowired
	private KafkaTemplate<String, PaymentNotificationRequest> kafkaTemplate;

	public void sendNotification(PaymentNotificationRequest request) {
		logger.info("Sending notification with body = < {} >", request);
		Message<PaymentNotificationRequest> message = MessageBuilder.withPayload(request)
				.setHeader(TOPIC, "payment-topic").build();

		kafkaTemplate.send(message);
	}

}
