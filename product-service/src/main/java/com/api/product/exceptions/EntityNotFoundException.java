package com.api.product.exceptions;

public class EntityNotFoundException extends RuntimeException {

	private String msg;

	public EntityNotFoundException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
