package com.api.product.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.product.entity.Product;
import com.api.product.exceptions.EntityNotFoundException;
import com.api.product.exceptions.ProductPurchaseException;
import com.api.product.record.ProductPurchaseRequest;
import com.api.product.record.ProductPurchaseResponse;
import com.api.product.record.ProductRequest;
import com.api.product.record.ProductResponse;
import com.api.product.repository.ProductRepository;

import jakarta.validation.Valid;


@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductMapper mapper;

	public Integer createProduct(@Valid ProductRequest request) {
		Product product = mapper.toProduct(request);
		return productRepository.save(product).getId();
	}

	public List<ProductPurchaseResponse> purchaseProducts(List<ProductPurchaseRequest> request) {
		var productIds = request.stream().map(ProductPurchaseRequest::productId).toList();
		var storedProducts = productRepository.findAllByIdInOrderById(productIds);
		if (productIds.size() != storedProducts.size()) {
			throw new ProductPurchaseException("One or more products doesnot exists.");
		}

		var storeRequest = request.stream().sorted((s1, s2) -> s1.productId() - s2.productId()).toList();
		var purchasedProducts = new ArrayList<ProductPurchaseResponse>();

		for (int i = 0; i < storedProducts.size(); i++) {

			var product = storedProducts.get(i);
			var productRequest = storeRequest.get(i);
			if (product.getAvailableQuantity() < productRequest.quantity()) {
				throw new ProductPurchaseException(
						"Insufficicent stock quantity for product with id" + product.getId());
			}
			var newAvailableQuantity = product.getAvailableQuantity() - productRequest.quantity();
			product.setAvailableQuantity(newAvailableQuantity);
			productRepository.save(product);
			purchasedProducts.add(mapper.toproductPurchaseResponse(product, productRequest.quantity()));
		}

		return purchasedProducts;
	}

	public ProductResponse findById(Integer productId) {
		return productRepository.findById(productId).map(mapper::toProductResponse)
				.orElseThrow(() -> new EntityNotFoundException("Product not found with ID:: " + productId));
	}

	public List<ProductResponse> findAll() {
		return productRepository.findAll().stream().map(mapper::toProductResponse).collect(Collectors.toList());
	}

}
