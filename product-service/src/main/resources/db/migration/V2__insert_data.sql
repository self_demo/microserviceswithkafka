/*INSERT INTO category (description, name)
VALUES 
    ('Electronics gadgets', 'Electronics'),
    ('Books of various genres', 'Books'),
    ('Household appliances', 'Home Appliances'),
    ('Clothing and accessories', 'Fashion'),
    ('Furniture and decor', 'Home Decor'),
    ('Sports equipment', 'Sports'),
    ('Health and wellness products', 'Health'),
    ('Toys and games', 'Toys');

    
    
INSERT INTO product (description, name, available_quantity, price, category_id)
VALUES 
    ('Smartphone with 64GB storage', 'Smartphone', 100, 299.99, 2401),
    ('Science fiction novel', 'Sci-Fi Book', 200, 15.99, 2451),
    ('Vacuum cleaner', 'Vacuum Cleaner', 50, 99.99, 2501),
    ('Men''s formal shirt', 'Formal Shirt', 150, 49.99, 2551),
    ('Wall clock with pendulum', 'Pendulum Clock', 20, 79.99, 2601),
    ('Football kit', 'Football Kit', 30, 199.99, 2651),
    ('Football shoes', 'Football Shoes', 50, 79.99, 2651),
    ('Basketball', 'Basketball', 30, 29.99, 2651),
    ('Vitamin supplements', 'Vitamin Supplements', 300, 29.99, 2701),
    ('Board game for kids', 'Kids Board Game', 80, 24.99, 2751);
    */