-- Create the category table if it doesn't exist
CREATE TABLE IF NOT EXISTS category (
    id INTEGER NOT NULL PRIMARY KEY,
    description VARCHAR(255),
    name VARCHAR(255)
);

-- Create the product table if it doesn't exist
CREATE TABLE IF NOT EXISTS product (
    id INTEGER NOT NULL PRIMARY KEY,
    description VARCHAR(255),
    name VARCHAR(255),
    available_quantity DOUBLE PRECISION NOT NULL,
    price NUMERIC(38,2),
    category_id INTEGER,
    CONSTRAINT fk_category_product FOREIGN KEY (category_id) REFERENCES category (id)
);

-- Create sequences if they don't exist
CREATE SEQUENCE IF NOT EXISTS category_seq INCREMENT BY 50;
CREATE SEQUENCE IF NOT EXISTS product_seq INCREMENT BY 50;

-- Optionally, set the default value for the id columns to use the sequences
ALTER TABLE category ALTER COLUMN id SET DEFAULT nextval('category_seq');
ALTER TABLE product ALTER COLUMN id SET DEFAULT nextval('product_seq');
